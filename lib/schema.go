package lib

type Schema struct {
	ToolName      string         `json:"tool_name,omitempty"`
	Resources     []ResourceData `json:"resources,omitempty"`
	ListenerQueue string         `json:"tool_listener_queue,omitempty"`
}

type ResourceData struct {
	Name           string           `json:"name,omitempty"`
	CreateForm     []FormElement    `json:"create_form,omitempty"`
	ActionsAllowed []ActionsAllowed `json:"actions_allowed,omitempty"`
}

type ActionsAllowed struct {
	Name            string        `json:"name,omitempty"`
	HttpRequestType string        `json:"http_request_type,omitempty"`
	CreateForm      []FormElement `json:"action_create_form,omitempty"`
	RequiredRoles   []string      `json:"required_roles,omitempty"`
}

type FormElement struct {
	Name        string `json:"field_name,omitempty"`
	Type        string `json:"field_type,omitempty"`
	Description string `json:"description,omitempty"`
	Required    bool   `json:"required,omitempty"`
}
