package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"

	"gitlab.com/indiez/cosmos-go-libs/queue/entities"
	sqsQueue "gitlab.com/indiez/cosmos-go-libs/queue/providers/sqs"
)

var (
	awsProfile  = flag.String("profile", "default", "Profile value for the AWS credentials in ~/.aws/credentials")
	queue       = flag.String("queue", "https://sqs.us-west-2.amazonaws.com/692957975310/cosmos-queue-stage-slack_alerts", "SQS queue name. Required to run consumer")
	topicARN    = flag.String("topic-arn", "arn:aws:sns:us-west-2:692957975310:cosmos-topic-stage-slack_alerts", "Topic ARN. Required to run producer")
	numIter     = flag.Int("iterations", 10, "Number of times to perform request")
	runProducer = flag.Bool("run-producer", false, "Command to run producer")
	runConsumer = flag.Bool("run-consumer", false, "Command to run consumer")
)

func main() {
	flag.Parse()

	printConfig()

	if *runProducer && *runConsumer {
		fmt.Println("Please pass either --run-producer or --run-consumer")
		os.Exit(0)
	} else {
		fmt.Println("Please pass either --run-producer or --run-consumer")
		os.Exit(0)
	}

	if *runProducer {
		for i := 0; i < *numIter; i++ {
			Produce()
		}
	}

	if *runConsumer {
		for i := 0; i < *numIter; i++ {
			Consume()
		}
	}
}

func Consume() {
	con := sqsQueue.NewConsumer(*queue, 10, 20, 0)
	messages, totalCount, err := con.Consume()
	reportError(err)

	if err == nil {
		fmt.Printf("Fetched %d messages", totalCount)
		for _, message := range messages {
			fmt.Print(message.Payload)
			con.DeleteMessage(message)
		}
		fmt.Println()
	}

}

func Produce() {
	producer := sqsQueue.NewProducer(*topicARN)

	sample_payload := map[string]interface{}{
		"event_name":      "created",
		"organization_id": 20,
	}

	json_sample_payload, err := json.Marshal(sample_payload)
	reportError(err)

	m := &entities.ProducerMessage{
		Key:     "",
		Payload: string(json_sample_payload),
	}

	response, err := producer.Produce(m)
	reportError(err)
	fmt.Println(response)
}

func reportError(err error) {
	if err != nil {
		fmt.Println("Error")
		fmt.Println(err)
	}
}

func printConfig() {
	fmt.Println("Running with configuration: ")
	fmt.Println("AWS Profile:		", *awsProfile)
	fmt.Println("SQS Queue:		", *queue)
	fmt.Println("SNS Topic ARN:		", *topicARN)
	fmt.Println("Running Producer?:	", *runProducer)
	fmt.Println("Running Consumer?: 	", *runConsumer)
	fmt.Println("======================================")
}
