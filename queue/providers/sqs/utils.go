package sqs

import "github.com/aws/aws-sdk-go/aws/session"

// @todo: fetch profileToUse from config
func CreateSession() *session.Session {
	return session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Profile:           "indiez",
	}))
}
