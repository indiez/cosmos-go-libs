package sqs

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/indiez/cosmos-go-libs/queue/entities"
)

type consumer struct {
	queueURL            *string
	maxNumberOfMessages *int64
	visibilityTimeout   *int64
	waitTimeSeconds     *int64

	service *sqs.SQS
}

func NewConsumer(queueURL string, maxNumberOfMessages int64, visibilityTimeout int64, waitTimeSeconds int64) *consumer {
	session := CreateSession()

	return &consumer{
		queueURL:            aws.String(queueURL),
		maxNumberOfMessages: aws.Int64(maxNumberOfMessages),
		visibilityTimeout:   aws.Int64(visibilityTimeout),
		waitTimeSeconds:     aws.Int64(waitTimeSeconds),
		service:             sqs.New(session),
	}
}

func (con *consumer) Consume() ([]*entities.ConsumerMessage, int, error) {
	result, err := con.service.ReceiveMessage(
		&sqs.ReceiveMessageInput{
			AttributeNames: []*string{
				aws.String(sqs.MessageSystemAttributeNameSentTimestamp),
				aws.String(sqs.MessageSystemAttributeNameSequenceNumber),
			},
			MessageAttributeNames: []*string{
				aws.String(sqs.QueueAttributeNameAll),
			},
			QueueUrl:            con.queueURL,
			MaxNumberOfMessages: con.maxNumberOfMessages,
			VisibilityTimeout:   con.visibilityTimeout,
			WaitTimeSeconds:     con.waitTimeSeconds,
		})

	if err != nil {
		return nil, 0, err
	}

	messages, totalCount := parseResults(result.Messages)
	return messages, totalCount, nil

}

func (con *consumer) DeleteMessage(message *entities.ConsumerMessage) {
	deleteMessageInput := &sqs.DeleteMessageInput{
		QueueUrl:      con.queueURL,
		ReceiptHandle: message.ProviderMeta.SqsMessageReceiptHandle,
	}
	con.service.DeleteMessage(deleteMessageInput)
}

func parseResults(messages []*sqs.Message) ([]*entities.ConsumerMessage, int) {
	totalCount := len(messages)
	parsedMessages := make([]*entities.ConsumerMessage, totalCount)

	for _, message := range messages {
		parsedMessages = append(
			parsedMessages,
			&entities.ConsumerMessage{
				Payload: *message.Body,
				Key:     "",
				ProviderMeta: &entities.ProviderMetadata{
					SqsMessageReceiptHandle: message.ReceiptHandle,
				},
			})
	}

	return parsedMessages, totalCount
}
