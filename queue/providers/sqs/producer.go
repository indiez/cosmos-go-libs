package sqs

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"gitlab.com/indiez/cosmos-go-libs/queue/entities"
)

type producer struct {
	topicArn *string
	service  *sns.SNS
}

func NewProducer(topicARN string) *producer {
	session := CreateSession()
	return &producer{
		topicArn: aws.String(topicARN),
		service:  sns.New(session),
	}
}

func (p *producer) Produce(message *entities.ProducerMessage) (*sns.PublishOutput, error) {
	params := &sns.PublishInput{
		Message:  aws.String(message.Payload),
		TopicArn: p.topicArn,
	}

	return p.service.Publish(params)
}
