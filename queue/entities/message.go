package entities

type ConsumerMessage struct {
	Payload      string
	Key          string
	ProviderMeta *ProviderMetadata
}

type ProviderMetadata struct {
	SqsMessageReceiptHandle *string
}

type ProducerMessage struct {
	Key     string
	Payload string
}
